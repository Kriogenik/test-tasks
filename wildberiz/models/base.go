package models

import "time"

type Base struct {
	ID        int64
	CreatedAt time.Time
	UpdatedAt time.Time `json:"-"`
	// DeletedAt *time.Time `sql:"index"`
}
