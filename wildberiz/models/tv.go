package models

type Tv struct {
	Base
	Brand        string
	Manufacturer string
	Model        string
	Year         int
}
