package router

import (
	"../handler"
	"github.com/labstack/echo"
)

func New() *echo.Echo {
	e := echo.New()

	e.GET("/search", handler.Search)
	e.POST("/add-tv", handler.AddTv)
	e.PUT("/update", handler.UpdateTv)

	return e

}
