package main

import (
	"./db"
	"./router"
)

func main() {

	e := router.New()

	db.Init()

	e.Start(":3000")

}
