package handler

import (
	"fmt"
	"net/http"

	"../db"
	"../models"
	"github.com/labstack/echo"
)

// Функцию для поиска
func Search(c echo.Context) error {
	brand := c.QueryParam("brand")

	arrTv := []models.Tv{}
	db := db.Manager()
	result, err := db.Query("select id, created_at, brand, manufacturer, model, year from tv where brand = ?", brand)
	if err != nil {
		panic(err)
	}
	defer result.Close()

	for result.Next() {
		tv := models.Tv{}

		err := result.Scan(&tv.ID, &tv.CreatedAt, &tv.Brand, &tv.Manufacturer, &tv.Model, &tv.Year)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}
		arrTv = append(arrTv, tv)
	}

	if len(arrTv) == 0 {
		return c.JSON(http.StatusNotFound, "Данных товаров не существует, ну или хз, что тут можно ещё написать")
	}

	return c.JSON(http.StatusOK, arrTv)
}

// добавляем телек
func AddTv(c echo.Context) error {
	tv := models.Tv{}

	if err := c.Bind(&tv); err != nil {
		return err
	}

	db := db.Manager()

	result, err := db.Exec("insert into tv (brand, manufacturer, model, year) values (?, ?, ?, ?)",
		tv.Brand, tv.Manufacturer, tv.Model, tv.Year)

	if err != nil {
		panic(err)
	}

	tv.ID, _ = result.LastInsertId()

	return c.JSON(http.StatusOK, tv)
}

func UpdateTv(c echo.Context) error {
	tv := models.Tv{}

	if err := c.Bind(&tv); err != nil {
		return err
	}

	db := db.Manager()

	_, err := db.Exec("update tv set brand = ?, manufacturer = ?, model = ?, year = ? where id = ?",
		tv.Brand, tv.Manufacturer, tv.Model, tv.Year, tv.ID)

	if err != nil {
		panic(err)
	}

	return c.JSON(http.StatusOK, tv)

}
