package db

import (
	"database/sql"
	"sync"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

var once sync.Once

func Init() {
	connect()
}

func connect() {
	DBMS := "mysql"
	USER := "root"
	PASS := "100496"
	PROTOCOL := "tcp(0.0.0.0:3306)"
	DBNAME := "tvDB"
	PARAMS := "?parseTime=true"

	CONNECT := USER + ":" + PASS + "@" + PROTOCOL + "/" + DBNAME + PARAMS

	var err error

	db, err = sql.Open(DBMS, CONNECT)

	if err != nil {
		panic(err.Error())
	}
}

func Manager() *sql.DB {
	once.Do(func() {
		db = &sql.DB{}
	})

	return db
}
