// Дан каталог на диске. В нём находятся различные файлы.
// Часть файлов имеет имя, заданное шаблоном:
//
// image-[name]-[timestamp].tar.gz
//
// например:
//
// image-x64_600_sspace_windows-20180726T161105.tar.gz
// image-unix_1100_engine-e2k-linux_vdb-20180726T041855.tar.gz
//
// Необходимо написать функцию, которая найдет в каталоге все файлы, попадающие под шаблон, и вернет список, содержащий только [name] из названий файлов.
// Для приведенных выше примеров, список должен содержать элементы:
// x64_600_sspace_windows
// unix_1100_engine-e2k-linux_vdb
//
// Обратите внимание, что [name] может также содержать символы дефиса.
// Список должен содержать уникальные элементы.

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
)

func main() {
	result := searchByPattern("./catalog")
	fmt.Println(result)
}

func searchByPattern(path string) []string {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}

	// image-[name]-[timestamp].tar.gz
	template := regexp.MustCompile("image-(?P<name>\\S+?)-(?P<timestamp>\\d{8}T\\d{6})\\.tar\\.gz")
	prevName := ""
	var names []string

	for _, file := range files {
		fileName := file.Name()

		if file.IsDir() || !template.MatchString(fileName) {
			continue
		}

		name := template.FindStringSubmatch(fileName)[1]

		if name == prevName {
			continue
		}

		names = append(names, name)
		prevName = name
	}

	return names
}
