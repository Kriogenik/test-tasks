// Дан текстовый log-файл, в котором могут появляться события Segmentation fault , пример:
//
// ...
// Jul 26 15:58:06 scan3 journal: [debug]:drweb-se F[14189]: DrWebCallDwShieldEx: command is not implemented: 16
// Jul 26 15:58:06 scan3 drweb-se[3287]: [debug]:Client 0xa6105a0: disconnected
// Jul 26 15:58:06 scan3 drweb-se[3287]: [err]:F-14211: Dump: BEGIN
// Jul 26 15:58:06 scan3 drweb-se[3287]: [err]:F-14211: Dump: Segmentation fault at 00000000
// Jul 26 15:58:06 scan3 drweb-se[3287]: [err]:F-14211: Dump: EAX=E4399DEC EBX=00000000 ECX=00000000 EDX=00000000
// Jul 26 15:58:06 scan3 drweb-se[3287]: [err]:F-14211: Dump: ESI=F6D84000 EDI=F6C16E74 EBP=F6C16E94 EFL=00010246
// Jul 26 15:58:06 scan3 drweb-se[3287]: [err]:F-14211: Dump: ESP=F6C16E3C (746E C1F6 0040 D8F6 EC9D 39E4 F45A 3AF7 EC9D 39E4 A222 1E00 1B00 0000 946E C1F6)
// Jul 26 15:58:06 scan3 drweb-se[3287]: [err]:F-14211: Dump: EIP=F73A4EBC (8A1A 8A15 7970 06F7 8A9B 586D 06F7 00D3 8A50 0100 DA8D 5802 0FB6 7301 31C0 8A86)
// Jul 26 15:58:06 scan3 drweb-se[3287]: [info]:Engine child with pid 14211 terminated
// Jul 26 15:58:06 scan3 drweb-se[3287]: [debug]:Cemetery: clean up after pid=14211
// Jul 26 15:58:06 scan3 drweb-se[3287]: [debug]:Client 0xa5f6ec0: disconnected
// ...
//
// Написать функцию-парсер таких log-файлов, который будет находить все такие события и возвращать структуру со следующей информацией (по примеру выше):
//
// PID: 14211
// SF_AT: 00000000
// SF_TEXT:
// EAX=E4399DEC EBX=00000000 ECX=00000000 EDX=00000000
// ESI=F6D84000 EDI=F6C16E74 EBP=F6C16E94 EFL=00010246
// ESP=F6C16E3C (746E C1F6 0040 D8F6 EC9D 39E4 F45A 3AF7 EC9D 39E4 A222 1E00 1B00 0000 946E C1F6)
// EIP=F73A4EBC (8A1A 8A15 7970 06F7 8A9B 586D 06F7 00D3 8A50 0100 DA8D 5802 0FB6 7301 31C0 8A86)

package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	s "strings"
)

// Создаем структуру для заполнения
type Quest struct {
	PID     string
	SF_AT   string
	SF_TEXT []string
}

func main() {
	// Открываем каталог, где могут находится все log-файлы
	files, err := ioutil.ReadDir("./catalog")
	if err != nil {
		log.Fatal(err)
	}
	// Поочередно отправляем их в функцию, которая будет заниматься поиском ошибок
	for _, file := range files {
		craft(file)
	}
}

func craft(fileInfo os.FileInfo) {
	// Открываем нужный файл
	file, err := os.Open("./catalog" + "/" + fileInfo.Name())
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	quest := Quest{}

	// Бегаем по строкам, существующих в файлах
	for scanner.Scan() {
		// Проверяем на EAX и добавляем его в массив
		checkEAX := s.Contains(scanner.Text(), "EAX")
		if checkEAX == true {
			arr := s.Split(scanner.Text(), ":")
			last := len(arr) - 1
			quest.SF_TEXT = append(quest.SF_TEXT, arr[last])
		}

		// Проверяем на ESI и добавляем его в массив
		checkESI := s.Contains(scanner.Text(), "ESI")
		if checkESI == true {
			arr := s.Split(scanner.Text(), ":")
			last := len(arr) - 1
			quest.SF_TEXT = append(quest.SF_TEXT, arr[last])
		}

		// Проверяем на ESP и добавляем его в массив
		checkESP := s.Contains(scanner.Text(), "ESP")
		if checkESP == true {
			arr := s.Split(scanner.Text(), ":")
			last := len(arr) - 1
			quest.SF_TEXT = append(quest.SF_TEXT, arr[last])
		}

		// Проверяем на EIP и добавляем его в массив
		checkEIP := s.Contains(scanner.Text(), "EIP")
		if checkEIP == true {
			arr := s.Split(scanner.Text(), ":")
			last := len(arr) - 1
			quest.SF_TEXT = append(quest.SF_TEXT, arr[last])
		}
		// Ищем строку с Segmentation fault и находим в нём SF_AT
		checkSegmentation := s.Contains(scanner.Text(), "Segmentation fault")
		if checkSegmentation == true {
			arr := s.Split(scanner.Text(), " ")
			last := len(arr) - 1
			quest.SF_AT = arr[last]
		}
		// Ищем PID
		checkPID := s.Contains(scanner.Text(), "pid=")
		if checkPID == true {
			splitArr := s.Split(scanner.Text(), " ")
			for _, element := range splitArr {
				if s.Contains(element, "pid=") == true {
					arr := s.Split(element, "=")
					quest.PID = arr[1]
				}
			}
		}

	}
	// выводим
	fmt.Println(quest)

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	// ГОТОВО
}
