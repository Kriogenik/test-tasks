package handlers

import (
	"fmt"
	"net/http"

	s "strings"

	"../maps"
	"../models"

	"github.com/labstack/echo"
)

// Принимаем данные в виде JSON
func Accept(c echo.Context) error {
	mainJson := models.Main{}
	// Вживляем JSON в переменную
	if err := c.Bind(&mainJson); err != nil {
		return err
	}
	// Отправляем полученную структуру в функцию magic и обратно получаем высчитанный рейтинг
	points := Magic(mainJson)

	// Выводим рейтинг
	fmt.Println(points)
	return c.JSON(http.StatusOK, points)
}

// Получаем структуру для работы с ней
func Magic(main models.Main) int {
	// Рейтинг по умолчанию
	points := 50
	extensions := []string{"exe", "dll", "pdf"}

	// Разибваем URL и высчитываем индекс последнего элемента
	url := s.ToLower(main.Url)
	urlParts := s.Split(main.Url, ".")
	urlExtension := urlParts[len(urlExtension)-1]

	// Добавляем баллы
	for _, e := range extensions {
		if e == urlExtension {
			points += 15
			break
		} else if s.Contains(url, e) {
			points += 5
			break
		}
	}

	// Делаем проверку на определенное число
	if main.Info.As == 666 {
		points += 50
	}

	// Ищем в мапе нужное количество баллов и прибавляем
	points += maps.Count(main.Info.Iso)
	for _, val := range arrUrl {
		points += maps.Count(val)
	}

	// возвращаем полученный результат
	return points
}
