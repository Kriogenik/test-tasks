package models

type Info struct {
	As      int
	As_org  string
	City    string
	Country string
	Iso     string
	Isp     string
	Org     string
}
