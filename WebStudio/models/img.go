package models

type Img struct {
	ID      int64
	Path    string
	Preview string
}
