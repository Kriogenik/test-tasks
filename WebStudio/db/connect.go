package db

import (
	"database/sql"
	"log"

	// _ "github.com/go-sql-driver/mysql"
	_ "github.com/mattn/go-sqlite3"
)

var db *sql.DB

func Init() {
	connect()
}

func connect() {

	var err error

	db, err = sql.Open("sqlite3", "WebStudio")

	if err != nil {
		log.Println("Ошибка при подключении к бд")
	}
}

func Manager() *sql.DB {

	return db
}
