package main

import (
	"encoding/json"
	"fmt"
	"image"
	"image/jpeg"
	"log"
	"net/http"
	"os"

	"./db"
	"./models"
	"github.com/nfnt/resize"
)

func main() {

	http.Handle("/img/", http.StripPrefix("/img", http.FileServer(http.Dir("./img"))))

	http.HandleFunc("/", hello)
	http.HandleFunc("/upload", UploadHandler)
	http.HandleFunc("/delete", Delete)

	fmt.Println("Серверв встал")

	db.Init()

	http.ListenAndServe(":3030", nil)

}

// удаляем нужную фотку (такой подход, правда , никогда не нравился, обычно делаю отдельную колонку в БД, где просто указывается, что инфа удалена, так меньше вероятности
// схлопотать проблемы в будущем)
func Delete(w http.ResponseWriter, r *http.Request) {

	if r.Method == http.MethodDelete {
		id := r.URL.Query().Get("id")

		db := db.Manager()

		_, err := db.Exec("delete from Img where id = $1", id)
		if err != nil {
			log.Println(err)
		}

		http.Redirect(w, r, "/", 301)
	}

}

// Отправляет на клиент все объекты в виде json
func hello(w http.ResponseWriter, r *http.Request) {

	if r.Method == http.MethodGet {
		listImg := []models.Img{}

		db := db.Manager()

		rows, err := db.Query("select * from Img")
		if err != nil {
			log.Println(err)
		}

		for rows.Next() {
			img := models.Img{}
			err := rows.Scan(&img.ID, &img.Path, &img.Preview)
			if err != nil {
				log.Println(err)
				continue
			}

			listImg = append(listImg, img)
		}

		js, err := json.Marshal(listImg)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	}

}

// Загружам с клиента изображение, делаем к ней превьюшку
func UploadHandler(w http.ResponseWriter, r *http.Request) {

	if r.Method == http.MethodPost {
		saveImg := models.Img{}

		db := db.Manager()

		file, path, err := r.FormFile("file")
		if err != nil {
			log.Println("ошибка 1")
		}

		img, _, err := image.Decode(file)
		if err != nil {
			log.Println("ошибка 2")
		}

		newImg, err := os.Create("./img/" + path.Filename)
		if err != nil {
			log.Println("ошибка 3")
		}
		defer newImg.Close()

		jpeg.Encode(newImg, img, &jpeg.Options{jpeg.DefaultQuality})

		img = resize.Resize(300, 250, img, resize.Bicubic)
		imgPreview, _ := os.Create("./img/preview" + path.Filename)
		jpeg.Encode(imgPreview, img, nil)
		imgPreview.Close()

		saveImg.Path = "./img/" + path.Filename
		saveImg.Preview = "./img/preview" + path.Filename

		result, err := db.Exec("insert into Img (puth, preview) values ($1 , $2)",
			saveImg.Path, saveImg.Preview)

		if err != nil {
			log.Println("ошибка 4")
		}

		saveImg.ID, _ = result.LastInsertId()
	} else {
		log.Println("Ошибка ")
	}

}
