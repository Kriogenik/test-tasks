// Задача:
// Разработать простенький краулер, которому на вход подаётся URL и глубина сбора.
// Нужно загрузить страницу по этому URL, распарсить её и вытянуть все ссылки.
// Те ссылки, что ведут на тот же домен, попадают на вход краулеру с уменьшенной глубиной сбора.
// Если глубина сбора ноль, то ссылка не загружается.
// Полученные страницы нужно сохранить в какую-то встраиваемую базу ключ-значение.
// Например, LevelDB. Ключом будет выступать url, а значением страница.
//
// В задаче необходимо показать умение пользоваться примитивами Go,
// писать идиоматичный код и
// декомпозировать задачу на более мелкие логические составляющие.

package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"sync"

	"github.com/go-redis/redis"
)

func main() {

	// Запускаем первоночально

	deepSearch("https://gobyexample.ru/", 4, nil)

}

// Главная функция, она занимается, как раз распределением всего
func deepSearch(link string, deep int, swg *sync.WaitGroup) {
	// Для приема нила в первый раз
	if swg != nil {
		defer swg.Done()
	}
	// Выводим просто все ссылки, добавил на всякий
	fmt.Println(link, deep)
	// условия для выключения рекурсии
	if deep == 0 {
		return
	}

	// получаем стуктуру url адреса
	mainURL, _ := url.Parse(link)
	// получаем содержимое сайта
	page, _ := getHtmlPage(link)

	// отправляем все на сохранение в бд
	save(link, page)
	// получаем все ссыылки со страницы
	links := findLinks(page)
	// создаем WaitGroup
	var wg sync.WaitGroup

	// начинаем работать с каждой ссылкой одновременно
	for _, l := range links {
		// разбиваю ссылку на структуру
		u, _ := url.Parse(l)

		// в этом большом блоке я сам досконально не смог разобраться (ЧЕСТНОСТЬ ЗАЛОГ УСПЕХА)
		// Но здесь я как раз пытаюсь разобрать ссылку, затем собрать, для получения абсолютной ссыылки
		// Вроде как все работает правильно
		if u.Host == mainURL.Host || u.Host == "" {

			if u.Host == "" {
				p := strings.Split(mainURL.Path, "/")
				u.Path = strings.Join(p[:len(p)-2], "/") + "/" + l
			}

			if strings.HasPrefix(l, "./") {
				p := strings.Split(mainURL.Path, "/")
				u.Path = strings.Join(p[:len(p)-2], "/") + l[1:]
			}

			if strings.HasPrefix(l, "../") {
				c := strings.Count(l, "../")
				p := strings.Split(mainURL.Path, "/")
				u.Path = strings.Join(p[:len(p)-c-2], "/") + l[c*3-1:]
			}

			u.Scheme = mainURL.Scheme
			u.Host = mainURL.Host

			// много мнооого горутин
			wg.Add(1)
			go deepSearch(u.String(), deep-1, &wg)

		}
	}

	wg.Wait()
}

// Получаем содержимое с сайта
func getHtmlPage(link string) (string, error) {
	client := http.Client{}

	resp, err := client.Get(link)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	return string(body), nil
}

// получаем ссылки со страницы
func findLinks(page string) []string {
	// Сюда запихнем все получившиеся ссылки
	var links []string

	// сплитуем страницу
	for _, str := range strings.Split(page, " href=\"")[1:] {
		link := strings.SplitN(str, "\"", 2)[0]

		// если в теге a, вместо ссылки окажется телефон, якорь или почта, то пропускаем её
		if strings.HasPrefix(link, "tel:") || strings.HasPrefix(link, "mailto:") || strings.HasPrefix(link, "#") {
			continue
		}

		// запихиваем получившиеся ссылки в срез
		links = append(links, link)
	}

	// возвращаем срез ссылок
	return links
}

// функция для сохранения ссылок
func save(link string, page string) {
	// подключаемся к бд
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	// Проверяем, есть ли уже такая ссылка (ключ) в бд
	// Если не находим подобного, то сохраняем его в бд
	_, err := client.Get(link).Result()
	if err == redis.Nil {
		err := client.Set(link, page, 0).Err()
		if err != nil {
			panic(err)
		}
	}

}
